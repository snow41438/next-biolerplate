import Head from 'next/head'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Carrer Bay</title>
      <meta name='keywords' content='web development, programming'/>
      </Head>
      <h1>Snow white</h1>
    </div>
  )
}
