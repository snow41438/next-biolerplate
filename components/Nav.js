import React from 'react'
import navStyles from '../styles/Nav.module.css'
import Link from 'next/link'
import styled from 'styled-components'


const Button = styled.button``

const Nav = () => {
    return (
        <div className={navStyles.nav}>
            <ul>
                <li>
                    <Link href='/'>Home</Link>
                </li>
                <li>
                    <Link href='/about'>About</Link>
                </li>
            </ul>
        </div>
    )
}

export default Nav