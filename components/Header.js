import React from 'react'
import headerStyles from "../styles/Header.module.css"

const Header = () => {
    return (
        <div>
            <h1 className={headerStyles.title}><span>Web Dev</span></h1>
            {/* <style jsx>
                {`
                .title{
                    color:red
                }
                `}
            </style> */}
            <p className={headerStyles.description}>hi this is snow here nice to meet you have a good day</p>
        </div>
    )
}

export default Header