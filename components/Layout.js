import React from 'react'
import styles from '../styles/Layout.module.css'
import Nav from './Nav'
import Header from './Header'
import HeroHeader from './HeroHeader'
import styled from 'styled-components'


const Wrapper = styled.div`
background: transparent;
`
const Button = styled.button`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0 1em;
  padding: 0.25em 1em;
`
const Layout = ({children}) => {
    return (
        <>
        <HeroHeader/>
        <div className={styles.container}>
           <main className={styles.main}>
            <Header/>
            <Nav/>
            {children}
            </main> 
        </div>
        </>
    )
}

export default Layout